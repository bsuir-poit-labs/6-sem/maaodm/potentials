﻿using System.Drawing;

namespace Potentials.Model
{
    public class SeparatingFunction
    {
        private int _coefX;
        private int _coefY;
        private int _coefXy;
        private int _coefC;

        public SeparatingFunction(int coefX, int coefY, int coefXy, int coefC)
        {
            _coefX = coefX;
            _coefY = coefY;
            _coefXy = coefXy;
            _coefC = coefC;
        }

        public SeparatingFunction()
        {
            _coefX = 0;
            _coefY = 0;
            _coefXy = 0;
            _coefC = 0;
        }

        public int GetValue(Point point)
        {
            return _coefC + _coefX * point.X + _coefY * point.Y + _coefXy * point.X * point.Y;
        }

        public double GetY(double x)
        {
            return -(_coefC + _coefX * x) / (_coefXy * x + _coefY);
        }

        public override string ToString()
        {
            if (_coefXy != 0)
            {
                return "y=(" + -_coefX + "*x" + (-_coefC < 0 ? "" : "+") + -_coefC + ")/(" +
                       _coefXy +
                       "*x" + (_coefY < 0 ? "" : "+") + _coefY + ")";
            }

            if (_coefY != 0)
            {
                return "y=" + -(double) _coefX / _coefY + "*x" +
                       (-(double) _coefC / _coefY < 0 ? "" : "+") + -(double) _coefC / _coefY;
            }

            return "x=" + -(double) _coefC / _coefX;
        }

        public static SeparatingFunction operator +(SeparatingFunction first, SeparatingFunction second)
        {
            return new(first._coefX + second._coefX, first._coefY + second._coefY,
                first._coefXy + second._coefXy, first._coefC + second._coefC);
        }

        public static SeparatingFunction operator *(int coef, SeparatingFunction function)
        {
            return new(coef * function._coefX, coef * function._coefY,
                coef * function._coefXy, coef * function._coefC);
        }
    }
}