﻿using System;
using System.Drawing;

namespace Potentials.Model
{
    public class Potentials
    {
        private const int ClassCount = 2;
        private const int MaxIterationsCount = 100;

        private int _correction;

        public bool IsWarning { get; private set; }

        public SeparatingFunction CalculateFunction(Point[][] points)
        {
            var result = new SeparatingFunction();
            _correction = 1;
            IsWarning = false;
            bool isIterate;
            var iterationsCount = 0;

            do
            {
                isIterate = Iterate(points, ref result);
                iterationsCount++;
            } while (isIterate & iterationsCount < MaxIterationsCount);

            IsWarning = iterationsCount == MaxIterationsCount;

            return result;
        }
        
        private bool Iterate(Point[][] teachingPoints, ref SeparatingFunction result)
        {
            var nextIteration = false;

            if (teachingPoints.Length != ClassCount) throw new Exception();

            for (var classNumber = 0; classNumber < ClassCount; classNumber++)
            {
                for (var i = 0; i < teachingPoints[classNumber].Length; i++)
                {
                    var index = (i + 1) % teachingPoints[classNumber].Length;
                    var nextClassNumber = index == 0 ? (classNumber + 1) % ClassCount : classNumber;
                    var nextPoint = teachingPoints[nextClassNumber][index];
                    
                    result += _correction * PartFunction(teachingPoints[classNumber][i]);
                    _correction = GetNewCorrection(nextPoint, result, nextClassNumber);
                    
                    nextIteration = _correction != 0;
                }
            }

            return nextIteration;
        }

        private static int GetNewCorrection(Point nextPoint, SeparatingFunction result, int nextClassNumber)
        {
            var functionValue = result.GetValue(nextPoint);
            switch (functionValue)
            {
                case <= 0 when nextClassNumber == 0:
                    return 1;
                case > 0 when nextClassNumber == 1:
                    return -1;
                default:
                    return 0;
            }
        }


        private static SeparatingFunction PartFunction(Point point)
        {
            return new(4 * point.X, 4 * point.Y, 16 * point.X * point.Y, 1);
        }
    }
}