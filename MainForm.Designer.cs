﻿namespace Potentials
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

         #region Windows Form Designer generated code

         /// <summary>
         /// Required method for Designer support - do not modify
         /// the contents of this method with the code editor.
         /// </summary>
         private void InitializeComponent()
         {
             this.panel = new System.Windows.Forms.Panel();
             this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
             this.gbTest = new System.Windows.Forms.GroupBox();
             this.btnFindClass = new System.Windows.Forms.Button();
             this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
             this.label9 = new System.Windows.Forms.Label();
             this.nudTestX = new System.Windows.Forms.NumericUpDown();
             this.label10 = new System.Windows.Forms.Label();
             this.nudTestY = new System.Windows.Forms.NumericUpDown();
             this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
             this.groupBox1 = new System.Windows.Forms.GroupBox();
             this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
             this.groupBox2 = new System.Windows.Forms.GroupBox();
             this.label1 = new System.Windows.Forms.Label();
             this.nudY11 = new System.Windows.Forms.NumericUpDown();
             this.label2 = new System.Windows.Forms.Label();
             this.nudX11 = new System.Windows.Forms.NumericUpDown();
             this.groupBox3 = new System.Windows.Forms.GroupBox();
             this.label4 = new System.Windows.Forms.Label();
             this.nudY12 = new System.Windows.Forms.NumericUpDown();
             this.label5 = new System.Windows.Forms.Label();
             this.nudX12 = new System.Windows.Forms.NumericUpDown();
             this.groupBox4 = new System.Windows.Forms.GroupBox();
             this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
             this.groupBox5 = new System.Windows.Forms.GroupBox();
             this.label3 = new System.Windows.Forms.Label();
             this.nudY21 = new System.Windows.Forms.NumericUpDown();
             this.label6 = new System.Windows.Forms.Label();
             this.nudX21 = new System.Windows.Forms.NumericUpDown();
             this.groupBox6 = new System.Windows.Forms.GroupBox();
             this.label7 = new System.Windows.Forms.Label();
             this.nudY22 = new System.Windows.Forms.NumericUpDown();
             this.label8 = new System.Windows.Forms.Label();
             this.nudX22 = new System.Windows.Forms.NumericUpDown();
             this.btnCalculate = new System.Windows.Forms.Button();
             this.lbFunc = new System.Windows.Forms.Label();
             this.pictureBox = new System.Windows.Forms.PictureBox();
             this.panel.SuspendLayout();
             this.flowLayoutPanel2.SuspendLayout();
             this.gbTest.SuspendLayout();
             this.flowLayoutPanel5.SuspendLayout();
             ((System.ComponentModel.ISupportInitialize) (this.nudTestX)).BeginInit();
             ((System.ComponentModel.ISupportInitialize) (this.nudTestY)).BeginInit();
             this.flowLayoutPanel1.SuspendLayout();
             this.groupBox1.SuspendLayout();
             this.flowLayoutPanel3.SuspendLayout();
             this.groupBox2.SuspendLayout();
             ((System.ComponentModel.ISupportInitialize) (this.nudY11)).BeginInit();
             ((System.ComponentModel.ISupportInitialize) (this.nudX11)).BeginInit();
             this.groupBox3.SuspendLayout();
             ((System.ComponentModel.ISupportInitialize) (this.nudY12)).BeginInit();
             ((System.ComponentModel.ISupportInitialize) (this.nudX12)).BeginInit();
             this.groupBox4.SuspendLayout();
             this.flowLayoutPanel4.SuspendLayout();
             this.groupBox5.SuspendLayout();
             ((System.ComponentModel.ISupportInitialize) (this.nudY21)).BeginInit();
             ((System.ComponentModel.ISupportInitialize) (this.nudX21)).BeginInit();
             this.groupBox6.SuspendLayout();
             ((System.ComponentModel.ISupportInitialize) (this.nudY22)).BeginInit();
             ((System.ComponentModel.ISupportInitialize) (this.nudX22)).BeginInit();
             ((System.ComponentModel.ISupportInitialize) (this.pictureBox)).BeginInit();
             this.SuspendLayout();
             // 
             // panel
             // 
             this.panel.Controls.Add(this.flowLayoutPanel2);
             this.panel.Controls.Add(this.flowLayoutPanel1);
             this.panel.Dock = System.Windows.Forms.DockStyle.Left;
             this.panel.Location = new System.Drawing.Point(0, 0);
             this.panel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
             this.panel.Name = "panel";
             this.panel.Size = new System.Drawing.Size(331, 681);
             this.panel.TabIndex = 0;
             // 
             // flowLayoutPanel2
             // 
             this.flowLayoutPanel2.Controls.Add(this.gbTest);
             this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
             this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.BottomUp;
             this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 505);
             this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(14);
             this.flowLayoutPanel2.Name = "flowLayoutPanel2";
             this.flowLayoutPanel2.Padding = new System.Windows.Forms.Padding(14);
             this.flowLayoutPanel2.Size = new System.Drawing.Size(331, 176);
             this.flowLayoutPanel2.TabIndex = 1;
             // 
             // gbTest
             // 
             this.gbTest.Controls.Add(this.btnFindClass);
             this.gbTest.Controls.Add(this.flowLayoutPanel5);
             this.gbTest.Enabled = false;
             this.gbTest.Location = new System.Drawing.Point(17, 4);
             this.gbTest.Name = "gbTest";
             this.gbTest.Size = new System.Drawing.Size(300, 141);
             this.gbTest.TabIndex = 2;
             this.gbTest.TabStop = false;
             this.gbTest.Text = "Тестовая выборка";
             // 
             // btnFindClass
             // 
             this.btnFindClass.Location = new System.Drawing.Point(50, 95);
             this.btnFindClass.Margin = new System.Windows.Forms.Padding(62, 14, 14, 14);
             this.btnFindClass.Name = "btnFindClass";
             this.btnFindClass.Size = new System.Drawing.Size(193, 35);
             this.btnFindClass.TabIndex = 0;
             this.btnFindClass.Text = "Подобрать класс";
             this.btnFindClass.UseVisualStyleBackColor = true;
             this.btnFindClass.Click += new System.EventHandler(this.btnFindClass_Click);
             // 
             // flowLayoutPanel5
             // 
             this.flowLayoutPanel5.Controls.Add(this.label9);
             this.flowLayoutPanel5.Controls.Add(this.nudTestX);
             this.flowLayoutPanel5.Controls.Add(this.label10);
             this.flowLayoutPanel5.Controls.Add(this.nudTestY);
             this.flowLayoutPanel5.Location = new System.Drawing.Point(19, 33);
             this.flowLayoutPanel5.Name = "flowLayoutPanel5";
             this.flowLayoutPanel5.Size = new System.Drawing.Size(266, 52);
             this.flowLayoutPanel5.TabIndex = 1;
             // 
             // label9
             // 
             this.label9.Location = new System.Drawing.Point(7, 7);
             this.label9.Margin = new System.Windows.Forms.Padding(7);
             this.label9.Name = "label9";
             this.label9.Size = new System.Drawing.Size(25, 23);
             this.label9.TabIndex = 2;
             this.label9.Text = "X";
             // 
             // nudTestX
             // 
             this.nudTestX.Location = new System.Drawing.Point(39, 7);
             this.nudTestX.Margin = new System.Windows.Forms.Padding(0, 7, 7, 7);
             this.nudTestX.Name = "nudTestX";
             this.nudTestX.Size = new System.Drawing.Size(86, 34);
             this.nudTestX.TabIndex = 3;
             this.nudTestX.Value = new decimal(new int[] {2, 0, 0, 0});
             // 
             // label10
             // 
             this.label10.Location = new System.Drawing.Point(139, 7);
             this.label10.Margin = new System.Windows.Forms.Padding(7);
             this.label10.Name = "label10";
             this.label10.Size = new System.Drawing.Size(25, 23);
             this.label10.TabIndex = 4;
             this.label10.Text = "Y";
             // 
             // nudTestY
             // 
             this.nudTestY.Location = new System.Drawing.Point(171, 7);
             this.nudTestY.Margin = new System.Windows.Forms.Padding(0, 7, 7, 7);
             this.nudTestY.Name = "nudTestY";
             this.nudTestY.Size = new System.Drawing.Size(86, 34);
             this.nudTestY.TabIndex = 5;
             // 
             // flowLayoutPanel1
             // 
             this.flowLayoutPanel1.AllowDrop = true;
             this.flowLayoutPanel1.Controls.Add(this.groupBox1);
             this.flowLayoutPanel1.Controls.Add(this.groupBox4);
             this.flowLayoutPanel1.Controls.Add(this.btnCalculate);
             this.flowLayoutPanel1.Controls.Add(this.lbFunc);
             this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
             this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
             this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
             this.flowLayoutPanel1.Name = "flowLayoutPanel1";
             this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(14, 14, 14, 7);
             this.flowLayoutPanel1.Size = new System.Drawing.Size(331, 503);
             this.flowLayoutPanel1.TabIndex = 0;
             // 
             // groupBox1
             // 
             this.groupBox1.Controls.Add(this.flowLayoutPanel3);
             this.groupBox1.Location = new System.Drawing.Point(17, 17);
             this.groupBox1.Name = "groupBox1";
             this.groupBox1.Size = new System.Drawing.Size(300, 184);
             this.groupBox1.TabIndex = 7;
             this.groupBox1.TabStop = false;
             this.groupBox1.Text = "Класс 1";
             // 
             // flowLayoutPanel3
             // 
             this.flowLayoutPanel3.Controls.Add(this.groupBox2);
             this.flowLayoutPanel3.Controls.Add(this.groupBox3);
             this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
             this.flowLayoutPanel3.Location = new System.Drawing.Point(3, 30);
             this.flowLayoutPanel3.Name = "flowLayoutPanel3";
             this.flowLayoutPanel3.Padding = new System.Windows.Forms.Padding(7, 0, 7, 7);
             this.flowLayoutPanel3.Size = new System.Drawing.Size(294, 151);
             this.flowLayoutPanel3.TabIndex = 1;
             // 
             // groupBox2
             // 
             this.groupBox2.Controls.Add(this.label1);
             this.groupBox2.Controls.Add(this.nudY11);
             this.groupBox2.Controls.Add(this.label2);
             this.groupBox2.Controls.Add(this.nudX11);
             this.groupBox2.Location = new System.Drawing.Point(10, 3);
             this.groupBox2.Name = "groupBox2";
             this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 7, 3, 3);
             this.groupBox2.Size = new System.Drawing.Size(132, 134);
             this.groupBox2.TabIndex = 0;
             this.groupBox2.TabStop = false;
             this.groupBox2.Text = "Объект 1";
             // 
             // label1
             // 
             this.label1.Location = new System.Drawing.Point(6, 41);
             this.label1.Name = "label1";
             this.label1.Size = new System.Drawing.Size(25, 23);
             this.label1.TabIndex = 0;
             this.label1.Text = "X";
             // 
             // nudY11
             // 
             this.nudY11.Location = new System.Drawing.Point(32, 85);
             this.nudY11.Margin = new System.Windows.Forms.Padding(14);
             this.nudY11.Minimum = new decimal(new int[] {100, 0, 0, -2147483648});
             this.nudY11.Name = "nudY11";
             this.nudY11.Size = new System.Drawing.Size(86, 34);
             this.nudY11.TabIndex = 3;
             // 
             // label2
             // 
             this.label2.Location = new System.Drawing.Point(6, 87);
             this.label2.Name = "label2";
             this.label2.Size = new System.Drawing.Size(27, 23);
             this.label2.TabIndex = 2;
             this.label2.Text = "Y";
             // 
             // nudX11
             // 
             this.nudX11.Location = new System.Drawing.Point(32, 39);
             this.nudX11.Margin = new System.Windows.Forms.Padding(14);
             this.nudX11.Minimum = new decimal(new int[] {100, 0, 0, -2147483648});
             this.nudX11.Name = "nudX11";
             this.nudX11.Size = new System.Drawing.Size(86, 34);
             this.nudX11.TabIndex = 1;
             this.nudX11.Value = new decimal(new int[] {1, 0, 0, -2147483648});
             // 
             // groupBox3
             // 
             this.groupBox3.Controls.Add(this.label4);
             this.groupBox3.Controls.Add(this.nudY12);
             this.groupBox3.Controls.Add(this.label5);
             this.groupBox3.Controls.Add(this.nudX12);
             this.groupBox3.Location = new System.Drawing.Point(148, 3);
             this.groupBox3.Name = "groupBox3";
             this.groupBox3.Size = new System.Drawing.Size(134, 134);
             this.groupBox3.TabIndex = 1;
             this.groupBox3.TabStop = false;
             this.groupBox3.Text = "Объект 2";
             // 
             // label4
             // 
             this.label4.Location = new System.Drawing.Point(7, 41);
             this.label4.Name = "label4";
             this.label4.Size = new System.Drawing.Size(25, 23);
             this.label4.TabIndex = 4;
             this.label4.Text = "X";
             // 
             // nudY12
             // 
             this.nudY12.Location = new System.Drawing.Point(33, 85);
             this.nudY12.Margin = new System.Windows.Forms.Padding(14);
             this.nudY12.Minimum = new decimal(new int[] {100, 0, 0, -2147483648});
             this.nudY12.Name = "nudY12";
             this.nudY12.Size = new System.Drawing.Size(86, 34);
             this.nudY12.TabIndex = 7;
             this.nudY12.Value = new decimal(new int[] {1, 0, 0, 0});
             // 
             // label5
             // 
             this.label5.Location = new System.Drawing.Point(7, 87);
             this.label5.Name = "label5";
             this.label5.Size = new System.Drawing.Size(27, 23);
             this.label5.TabIndex = 6;
             this.label5.Text = "Y";
             // 
             // nudX12
             // 
             this.nudX12.Location = new System.Drawing.Point(33, 39);
             this.nudX12.Margin = new System.Windows.Forms.Padding(14);
             this.nudX12.Minimum = new decimal(new int[] {100, 0, 0, -2147483648});
             this.nudX12.Name = "nudX12";
             this.nudX12.Size = new System.Drawing.Size(86, 34);
             this.nudX12.TabIndex = 5;
             this.nudX12.Value = new decimal(new int[] {1, 0, 0, 0});
             // 
             // groupBox4
             // 
             this.groupBox4.Controls.Add(this.flowLayoutPanel4);
             this.groupBox4.Location = new System.Drawing.Point(17, 207);
             this.groupBox4.Name = "groupBox4";
             this.groupBox4.Size = new System.Drawing.Size(300, 184);
             this.groupBox4.TabIndex = 8;
             this.groupBox4.TabStop = false;
             this.groupBox4.Text = "Класс 2";
             // 
             // flowLayoutPanel4
             // 
             this.flowLayoutPanel4.Controls.Add(this.groupBox5);
             this.flowLayoutPanel4.Controls.Add(this.groupBox6);
             this.flowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
             this.flowLayoutPanel4.Location = new System.Drawing.Point(3, 30);
             this.flowLayoutPanel4.Name = "flowLayoutPanel4";
             this.flowLayoutPanel4.Padding = new System.Windows.Forms.Padding(7, 0, 7, 7);
             this.flowLayoutPanel4.Size = new System.Drawing.Size(294, 151);
             this.flowLayoutPanel4.TabIndex = 1;
             // 
             // groupBox5
             // 
             this.groupBox5.Controls.Add(this.label3);
             this.groupBox5.Controls.Add(this.nudY21);
             this.groupBox5.Controls.Add(this.label6);
             this.groupBox5.Controls.Add(this.nudX21);
             this.groupBox5.Location = new System.Drawing.Point(10, 3);
             this.groupBox5.Name = "groupBox5";
             this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 7, 3, 3);
             this.groupBox5.Size = new System.Drawing.Size(132, 134);
             this.groupBox5.TabIndex = 0;
             this.groupBox5.TabStop = false;
             this.groupBox5.Text = "Объект 1";
             // 
             // label3
             // 
             this.label3.Location = new System.Drawing.Point(6, 41);
             this.label3.Name = "label3";
             this.label3.Size = new System.Drawing.Size(25, 23);
             this.label3.TabIndex = 0;
             this.label3.Text = "X";
             // 
             // nudY21
             // 
             this.nudY21.Location = new System.Drawing.Point(32, 85);
             this.nudY21.Margin = new System.Windows.Forms.Padding(14);
             this.nudY21.Minimum = new decimal(new int[] {100, 0, 0, -2147483648});
             this.nudY21.Name = "nudY21";
             this.nudY21.Size = new System.Drawing.Size(86, 34);
             this.nudY21.TabIndex = 3;
             // 
             // label6
             // 
             this.label6.Location = new System.Drawing.Point(6, 87);
             this.label6.Name = "label6";
             this.label6.Size = new System.Drawing.Size(27, 23);
             this.label6.TabIndex = 2;
             this.label6.Text = "Y";
             // 
             // nudX21
             // 
             this.nudX21.Location = new System.Drawing.Point(32, 39);
             this.nudX21.Margin = new System.Windows.Forms.Padding(14);
             this.nudX21.Minimum = new decimal(new int[] {100, 0, 0, -2147483648});
             this.nudX21.Name = "nudX21";
             this.nudX21.Size = new System.Drawing.Size(86, 34);
             this.nudX21.TabIndex = 1;
             this.nudX21.Value = new decimal(new int[] {2, 0, 0, 0});
             // 
             // groupBox6
             // 
             this.groupBox6.Controls.Add(this.label7);
             this.groupBox6.Controls.Add(this.nudY22);
             this.groupBox6.Controls.Add(this.label8);
             this.groupBox6.Controls.Add(this.nudX22);
             this.groupBox6.Location = new System.Drawing.Point(148, 3);
             this.groupBox6.Name = "groupBox6";
             this.groupBox6.Size = new System.Drawing.Size(134, 134);
             this.groupBox6.TabIndex = 1;
             this.groupBox6.TabStop = false;
             this.groupBox6.Text = "Объект 2";
             // 
             // label7
             // 
             this.label7.Location = new System.Drawing.Point(7, 41);
             this.label7.Name = "label7";
             this.label7.Size = new System.Drawing.Size(25, 23);
             this.label7.TabIndex = 4;
             this.label7.Text = "X";
             // 
             // nudY22
             // 
             this.nudY22.Location = new System.Drawing.Point(33, 85);
             this.nudY22.Margin = new System.Windows.Forms.Padding(14);
             this.nudY22.Minimum = new decimal(new int[] {100, 0, 0, -2147483648});
             this.nudY22.Name = "nudY22";
             this.nudY22.Size = new System.Drawing.Size(86, 34);
             this.nudY22.TabIndex = 7;
             this.nudY22.Value = new decimal(new int[] {2, 0, 0, -2147483648});
             // 
             // label8
             // 
             this.label8.Location = new System.Drawing.Point(7, 87);
             this.label8.Name = "label8";
             this.label8.Size = new System.Drawing.Size(27, 23);
             this.label8.TabIndex = 6;
             this.label8.Text = "Y";
             // 
             // nudX22
             // 
             this.nudX22.Location = new System.Drawing.Point(33, 39);
             this.nudX22.Margin = new System.Windows.Forms.Padding(14);
             this.nudX22.Minimum = new decimal(new int[] {100, 0, 0, -2147483648});
             this.nudX22.Name = "nudX22";
             this.nudX22.Size = new System.Drawing.Size(86, 34);
             this.nudX22.TabIndex = 5;
             this.nudX22.Value = new decimal(new int[] {1, 0, 0, 0});
             // 
             // btnCalculate
             // 
             this.btnCalculate.Location = new System.Drawing.Point(97, 408);
             this.btnCalculate.Margin = new System.Windows.Forms.Padding(83, 14, 3, 3);
             this.btnCalculate.Name = "btnCalculate";
             this.btnCalculate.Size = new System.Drawing.Size(142, 36);
             this.btnCalculate.TabIndex = 6;
             this.btnCalculate.Text = "Обучить";
             this.btnCalculate.UseVisualStyleBackColor = true;
             this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
             // 
             // lbFunc
             // 
             this.lbFunc.Location = new System.Drawing.Point(17, 461);
             this.lbFunc.Margin = new System.Windows.Forms.Padding(3, 14, 3, 0);
             this.lbFunc.Name = "lbFunc";
             this.lbFunc.Size = new System.Drawing.Size(300, 30);
             this.lbFunc.TabIndex = 9;
             this.lbFunc.TextAlign = System.Drawing.ContentAlignment.TopCenter;
             // 
             // pictureBox
             // 
             this.pictureBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
             this.pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
             this.pictureBox.Location = new System.Drawing.Point(331, 0);
             this.pictureBox.Name = "pictureBox";
             this.pictureBox.Size = new System.Drawing.Size(687, 681);
             this.pictureBox.TabIndex = 1;
             this.pictureBox.TabStop = false;
             // 
             // MainForm
             // 
             this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
             this.ClientSize = new System.Drawing.Size(1018, 681);
             this.Controls.Add(this.pictureBox);
             this.Controls.Add(this.panel);
             this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
             this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
             this.Name = "MainForm";
             this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
             this.Text = "Потенциалы";
             this.panel.ResumeLayout(false);
             this.flowLayoutPanel2.ResumeLayout(false);
             this.gbTest.ResumeLayout(false);
             this.flowLayoutPanel5.ResumeLayout(false);
             ((System.ComponentModel.ISupportInitialize) (this.nudTestX)).EndInit();
             ((System.ComponentModel.ISupportInitialize) (this.nudTestY)).EndInit();
             this.flowLayoutPanel1.ResumeLayout(false);
             this.groupBox1.ResumeLayout(false);
             this.flowLayoutPanel3.ResumeLayout(false);
             this.groupBox2.ResumeLayout(false);
             ((System.ComponentModel.ISupportInitialize) (this.nudY11)).EndInit();
             ((System.ComponentModel.ISupportInitialize) (this.nudX11)).EndInit();
             this.groupBox3.ResumeLayout(false);
             ((System.ComponentModel.ISupportInitialize) (this.nudY12)).EndInit();
             ((System.ComponentModel.ISupportInitialize) (this.nudX12)).EndInit();
             this.groupBox4.ResumeLayout(false);
             this.flowLayoutPanel4.ResumeLayout(false);
             this.groupBox5.ResumeLayout(false);
             ((System.ComponentModel.ISupportInitialize) (this.nudY21)).EndInit();
             ((System.ComponentModel.ISupportInitialize) (this.nudX21)).EndInit();
             this.groupBox6.ResumeLayout(false);
             ((System.ComponentModel.ISupportInitialize) (this.nudY22)).EndInit();
             ((System.ComponentModel.ISupportInitialize) (this.nudX22)).EndInit();
             ((System.ComponentModel.ISupportInitialize) (this.pictureBox)).EndInit();
             this.ResumeLayout(false);
         }

         private System.Windows.Forms.Label lbFunc;

         private System.Windows.Forms.GroupBox gbTest;

         private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
         private System.Windows.Forms.Label label10;
         private System.Windows.Forms.Label label9;
         private System.Windows.Forms.NumericUpDown nudTestX;
         private System.Windows.Forms.NumericUpDown nudTestY;

         private System.Windows.Forms.PictureBox pictureBox;

         private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
         private System.Windows.Forms.GroupBox groupBox4;
         private System.Windows.Forms.GroupBox groupBox5;
         private System.Windows.Forms.GroupBox groupBox6;
         private System.Windows.Forms.Label label3;
         private System.Windows.Forms.Label label6;
         private System.Windows.Forms.Label label7;
         private System.Windows.Forms.Label label8;
         private System.Windows.Forms.NumericUpDown nudY21;
         private System.Windows.Forms.NumericUpDown nudX21;
         private System.Windows.Forms.NumericUpDown nudY22;
         private System.Windows.Forms.NumericUpDown nudX22;

         private System.Windows.Forms.GroupBox groupBox3;
         private System.Windows.Forms.Label label4;
         private System.Windows.Forms.Label label5;
         private System.Windows.Forms.NumericUpDown nudY12;
         private System.Windows.Forms.NumericUpDown nudX12;

         private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
         private System.Windows.Forms.GroupBox groupBox2;

         private System.Windows.Forms.GroupBox groupBox1;

         private System.Windows.Forms.Button btnFindClass;

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;

        private System.Windows.Forms.Button btnCalculate;

        private System.Windows.Forms.NumericUpDown nudY11;

        private System.Windows.Forms.Label label2;

        private System.Windows.Forms.NumericUpDown nudX11;

        private System.Windows.Forms.Label label1;

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;

        private System.Windows.Forms.Panel panel;

        #endregion
    }
}