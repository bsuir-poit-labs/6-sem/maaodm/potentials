﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Potentials.Model;

namespace Potentials
{
    public partial class MainForm : Form
    {
        private const float StepCount = 20;

        private List<Point>[] _points;
        private double _step;
        private SeparatingFunction _function;

        public MainForm()
        {
            InitializeComponent();
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            _step = pictureBox.Height / StepCount;
            _points = ControlsToValue();
            var potentials = new Model.Potentials();

            _function = potentials.CalculateFunction(new[] {_points[0].ToArray(), _points[1].ToArray()});

            if (!potentials.IsWarning)
            {
                lbFunc.Text = _function.ToString();
            }
            else
            {
                lbFunc.Text = string.Empty;
                MessageBox.Show(@"Невозможно построить разделяющую функцию!");
            }
            
            Draw(potentials);
            gbTest.Enabled = !potentials.IsWarning;
        }

        private List<Point>[] ControlsToValue()
        {
            var points = new List<Point>[2];

            points[0] = new List<Point>
            {
                new((int) nudX11.Value, (int) nudY11.Value),
                new((int) nudX12.Value, (int) nudY12.Value)
            };

            points[1] = new List<Point>
            {
                new((int) nudX21.Value, (int) nudY21.Value),
                new((int) nudX22.Value, (int) nudY22.Value)
            };

            return points;
        }

        private void Draw(Model.Potentials potentials)
        {
            var bitmap = new Bitmap(pictureBox.Width, pictureBox.Height);
            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                graphics.Clear(Color.White);
                if (!potentials.IsWarning)
                    DrawGraph(graphics);
                DrawEdges(graphics);
                DrawPoints(graphics, _points);
            }

            pictureBox.Image = bitmap;
        }

        private void DrawGraph(Graphics graphics)
        {
            var graphPen = new Pen(Color.Red, 2);
            var prevPoint = new Point(pictureBox.Width / 2 + (int) (-pictureBox.Width / 2.0 * _step),
                pictureBox.Height / 2 - (int) (_function.GetY(-pictureBox.Width / 2.0 / _step) * _step));
            
            for (var x = -pictureBox.Width / 2.0; x < pictureBox.Width / 2.0; x += 0.1)
            {
                var y = _function.GetY(x / _step);
                var nextPoint = new Point((int) (pictureBox.Width / 2.0 + x),
                    (int) (pictureBox.Height / 2.0 - y * _step));
                try
                {
                    if (Math.Abs(nextPoint.Y - prevPoint.Y) < pictureBox.Height)
                    {
                        graphics.DrawLine(graphPen, prevPoint, nextPoint);
                    }
                }
                catch (OverflowException)
                {
                    
                }

                prevPoint = nextPoint;
            }
        }

        private void DrawPoints(Graphics graphics, List<Point>[] points)
        {
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    DrawPoint(graphics, points[i][j], i);
                }
            }
        }

        private void DrawPoint(Graphics graphics, Point point, int classNumber)
        {
            graphics.FillEllipse(new SolidBrush(classNumber == 0 ? Color.ForestGreen : Color.Blue),
                (int) (pictureBox.Width / 2.0 + point.X * _step - 4),
                (int) (pictureBox.Height / 2.0 - point.Y * _step - 4), 9, 9);
        }

        private void DrawEdges(Graphics graphics)
        {
            graphics.DrawLine(Pens.Black, 0, pictureBox.Height / 2, pictureBox.Width, pictureBox.Height / 2);
            graphics.DrawLine(Pens.Black, pictureBox.Width / 2, 0, pictureBox.Width / 2, pictureBox.Height);
        }

        private void btnFindClass_Click(object sender, EventArgs e)
        {
            var point = new Point((int) nudTestX.Value, (int) nudTestY.Value);
            var classNumber = _function.GetValue(point) >= 0 ? 0 : 1;
            _points[classNumber].Add(point);
            MessageBox.Show($@"Точка ({point.X}, {point.Y}) принадлежит классу {classNumber}.");
            
            var bitmap = new Bitmap(pictureBox.Image);
            pictureBox.Image.Dispose();
            using (var graphics = Graphics.FromImage(bitmap))
            {
                DrawPoint(graphics, point, classNumber);
            }

            pictureBox.Image = bitmap;
        }
    }
}